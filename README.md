# ml_sports_better

Plan:
x Research state of the art (like Draftking)
x Decide on a database to pull from
x Pull Data
- Format data into data points
- Normalize data
- Decide what kind of ML to use
    - Input -> 
        Last Year Rank
        Last Year Team Stats 
        Last Year Players Stats 
        Current Year Team Stats Last X Games 
    - Output -> Winner Team

- Data Points:
    - For each game of the 2018/2019 season, we have two teams:
        - (Year before) Differences for aggregate team data points
        - (Year before) Differences for each player's stats per position
        - (This will change every game) Differences in points of games in the last 5 games, 0 or 1 for away/home
        - If new player, just use old player in the same position
    - Save to CSV

- Create NN architecture
- Tune NN hyperparameters

TODO:
- Add player stats from last year, diff between stats of each position
    - Pull roster and injury
    - Set roster to most popular roster
    - If injury, shift roster to second most popular roster
    - Once have roster, check player per game stats for stats
    - Take difference
- Expand to multiple years
- Creating data points yields different amounts of dp each time

- Make all data continuous, split in the end into train + valid + test
