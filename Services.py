import re
import requests
import unidecode
import numpy as np
import pandas as pd
pd.options.mode.chained_assignment = None

from bs4 import BeautifulSoup
from urllib.request import urlopen

from Global import *

def site_exists(url):
    """ Tests if URL is valid """
    request = requests.get(url)
    return requests.get(url).status_code == 200


def normalize_str(s):
    """ Normalizes string if it has unicode special characters """
    return unidecode.unidecode(
        s.encode('latin1').decode('unicode_escape').encode('latin1').decode('utf-8')
    )


def get_df(s, args, str_type='url'):
    """ 
    Get pd dataframe from a URL / HTML string. Some code lent from:
    https://towardsdatascience.com/web-scraping-nba-stats-4b4f8c525994
    """
    # Error checking for URL
    if str_type == 'url':
        if not site_exists(s):
            if args.verbose: print("> Warning - invalid URL passed: {}".format(s))
            return None
        html = str(urlopen(s).read())
    elif str_type == 'html':
        html = s

    # Search for a table (<tr> tag), return first table
    if '<tr>' not in html:
        if args.verbose: print("> Warning - <tr> not found. Returning None from get_df")
        return None
    html = re.search("<tr.*</tr>", html)[0]

    # Get rows and headers
    soup = BeautifulSoup(html, features='html.parser')
    rows = soup.findAll('tr')
    headers = [th.getText().strip() for th in soup.findAll('tr')[0].findAll('th')]

    # Create dataframe
    stats = []
    for i in range(len(rows)):
        stat = []
        for td in rows[i].findAll(['a', 'td']):
            text = td.getText().strip()
            text = ' ' if text == '' else text
            stat.append(text)
        stats.append(stat)
    
    # Convert any strings -> floats if possible
    stats_1 = []
    for stat in stats:
        tmp_stat = []
        for x in stat:
            if re.match('^[0-9.]+$', x.strip()):
                x = float(x)
            tmp_stat.append(x)
        stats_1.append(tmp_stat)

    # Edge case - remove duplicates if from <a> and <td> tags
    stats_2 = []
    for l in stats_1:
        tmp_list = []
        if len(l) == 0:
            continue
        for i, n in enumerate(l):
            # If not a duplicate, append
            if isinstance(n, float) or str(n) == ' ' or i == 0 or \
               re.sub('\W+', '', str(n)) != re.sub('\W+', '', str(l[i-1])):
                tmp_list.append(n)
        stats_2.append(tmp_list)

    # Edge case - if this is starting lineups with a bunch of <a> tags, remove <a> tags at end
    if len(headers) == len(stats_2[0]) - 4:
        for i in range(len(stats_2)):
            stats_2[i] = stats_2[i][:len(stats_2[i])-5]
    
    # Edge case - if there is an index header, remove it
    if len(headers) == len(stats_2[0]) + 1:
        headers = headers[1:]

    # Edge case - if there is an overheader, set headers to the underheader
    if len(headers) != len(stats_2[0]):
        headers = [th.getText().strip() for th in soup.findAll('tr')[1].findAll('th')]
        if args.verbose: print("> Changed headers to: {}".format(headers))
        if len(headers) == len(stats_2[0]) + 1:
            headers = headers[1:]

    # Error handling - if mismatch between headers and data, something went wrong
    #print("Headers: {}".format(headers))
    #print("Stats: {}".format(stats_2))
    if len(headers) != len(stats_2[0]):
        if str_type == 'url':
            if args.verbose: print("> Warning: Mismatched headers + data url {}".format(s))
        else:
            if args.verbose: print("> Warning: Mismatched headers + data for html passed.")
        return None
    return pd.DataFrame(stats_2, columns=headers, index=None)


def get_dfs(url, labels, args):
    """ Return dictionary of pd dataframes if a page has multiple tables """
    if not site_exists(url):
        if args.verbose: print("> Warning - invalid URL passed: {}".format(url))
        return None

    # Fetch HTML from URL, and split by section
    soup = BeautifulSoup(urlopen(url), features='html.parser')
    htmls = [s.replace('\n', '') for s in soup.prettify().split('class="section_anchor"')]

    # Retrieve each section's table as a dataframe, put in pd_dict
    pd_dict = {}
    for label in labels:
        label_regex = 'data-label="' + label + '"'
        html = [html for html in htmls if re.search(label_regex, html)][0]
        html = re.search("<tr>.*</tr>", html)[0]
        stats = get_df(html, args, str_type='html')
        pd_dict[label] = stats
    return pd_dict


def retrieve_record(games, i, team):
    """ Retrieves record (23, 14) given a game index i """
    current_game = games.iloc[i]
    window       = games[:i]
    last_games = window.loc[(window['Visitor/Neutral'] == team) | (window['Home/Neutral'] == team)]
    wins, losses = 0, 0
    for index, game in last_games.iterrows():
        point_diff = game['PTS1'] - game['PTS2'] if game['Visitor/Neutral'] == team else \
                     game['PTS2'] - game['PTS1']
        if point_diff > 0:
            wins += 1
        else:
            losses += 1
    return wins, losses


def get_game_df(years, args):
    """ Creates dataframe containing all games from a year """
    game_df = None
    for year in years:
        if len(years) > 1: print("> Fetching games for year {}".format(year))
        for month in months:
            month_games = get_df(
                'https://www.basketball-reference.com/leagues/NBA_'+year+'_games-'+month+'.html', 
                args
            )
            if month_games is not None:
                game_df = month_games if game_df is None else \
                                         pd.concat([game_df, month_games], ignore_index=True)
        if len(years) > 1 and args.verbose: print(
            "> Fetched games for year {} (cumulative total {} games)".format(year, len(game_df.index))
        )

    # Sort by date
    game_df['Date'] = pd.to_datetime(game_df['Date'], format='%a, %b %d, %Y')
    game_df = game_df.sort_values(by='Date')

    # Drop unneeded columns
    game_df.drop(['Start (ET)', 'Attend.', 'Notes', ''], axis=1, inplace=True)
    game_df.columns = ['Date', 'Visitor/Neutral', 'PTS1', 'Home/Neutral', 'PTS2']

    # Drop columns with None entries
    game_df['PTS1'].replace('^\s*$', np.nan, inplace=True, regex=True)
    game_df.dropna(subset=['PTS1'], inplace=True)

    # Drop duplicates
    game_df.drop_duplicates(subset=None, keep='first', inplace=True)
    game_df.reset_index(drop=True, inplace=True)

    # Add records
    game_df['Wins1'], game_df['Losses1'], game_df['Wins2'], game_df['Losses2'] = np.nan, np.nan, np.nan, np.nan
    for index, game in game_df.iterrows():
        print("> Retrieving records for game {}/{}".format(index+1, len(game_df.index)), end='\r')
        wins1, losses1 = retrieve_record(game_df, index, game['Visitor/Neutral'])
        wins2, losses2 = retrieve_record(game_df, index, game['Home/Neutral'])
        game_df.at[index, 'Wins1'] = wins1
        game_df.at[index, 'Losses1'] = losses1
        game_df.at[index, 'Wins2'] = wins2
        game_df.at[index, 'Losses2'] = losses2
    print("")

    if len(years) > 1: print("> Total games found: {}".format(len(game_df.index)))
    return game_df


def get_roster_df(year, ppg_stats, args):
    """ Retrieve rosters by team for a given year """
    rosters = {}

    # Get rosters in a dictionary
    for team in abbrevs.values():
        print("> Retrieving lineup for team {}, year {} ({}/{})".format(
            team, year, list(abbrevs.values()).index(team)+1, len(abbrevs.values())
        ), end='\r')
        rosters[team] = get_dfs(
            'https://www.basketball-reference.com/teams/' + team + '/' + year + '_start.html',
            ['Starting Lineups'], args
        )
    print("")

    # Split 'Starting Lineup' string -> 5 players
    for team in abbrevs.values():
        if not rosters[team]:
            if args.verbose: print("> Warning: Team {} was not found for year {}".format(team, year))
            continue
        roster_history = rosters[team]['Starting Lineups']

        # Clean up df
        roster_history.drop([''], axis=1, inplace=True)
        roster_history = roster_history.replace('None', np.nan)
        roster_history.dropna(subset=['Starting Lineup'], inplace=True)
        roster_history = roster_history.reindex(
            roster_history.columns.to_list() + ['PF', 'SF', 'C', 'PG', 'SG'], axis=1
        )

        # Sort players -> positions
        for i in range(len(roster_history['Starting Lineup'])):
            row = roster_history['Starting Lineup'][i]
            players = re.findall(r'\w+\.\s\w+', row)
            positions = {'PF' : None, 'SF' : None, 'C'  : None, 'PG' : None, 'SG' : None}

            for player in players:
                # If player + team in ppg_stats, fetch position from ppg_stats
                position = ppg_stats.loc[
                    (ppg_stats['Player'] == unidecode.unidecode(player)) & (ppg_stats['Tm'] == team),
                    'Pos'
                ]
                # Backup - if just player in ppg_stats, fetch position from ppg_stats 
                if position.empty:
                    position = ppg_stats.loc[ppg_stats['Player'] == unidecode.unidecode(player), 'Pos']
                    if position.empty:
                        if args.verbose: print("> Warning: Could not find player {}".format(player))
                        continue

                position = position.iloc[0]
                # If special position, ignore (like PG-SF)
                if position not in positions.keys():
                    continue
                positions[position] = unidecode.unidecode(player)

            # Set positions in roster df
            roster_history['PF'][i] = positions['PF']
            roster_history['SF'][i] = positions['SF']
            roster_history['C'][i] = positions['C']
            roster_history['PG'][i] = positions['PG']
            roster_history['SG'][i] = positions['SG']
        rosters[team]['Starting Lineups'] = roster_history
    return rosters


def get_ppg_df(year, args):
    """ Fetch player-per-game stats in a df """
    ppg_url = 'https://www.basketball-reference.com/leagues/NBA_' + year + '_per_game.html'
    ppg_stats = get_df(ppg_url, args)

    ppg_stats['Player'] = [normalize_str(x) for x in ppg_stats['Player']]
    ppg_stats['Player'] = [
        re.sub(r'(.)[a-zA-Z\'.-]+\s+(\w+)\s*.*' ,r'\1. \2', x) for x in ppg_stats['Player']
    ]
    return ppg_stats


def get_rating_df(year, args):
    """ Fetch team ratings by year in a df """
    rating_url = 'https://www.basketball-reference.com/leagues/NBA_' + year + '_ratings.html'
    team_ranks  = get_df(rating_url, args)

    team_ranks.drop(['Conf', 'Div'], axis=1, inplace=True)    # Drop any unwanted/redundant data here
    return team_ranks


def get_tpg_df(year, args):
    """ Fetch team-per-game stats by year in a df """
    tpg_url = 'https://www.basketball-reference.com/leagues/NBA_' + year + '.html'
    tpg_stats = get_dfs(tpg_url, ['Team Per Game Stats'], args)['Team Per Game Stats']

    tpg_stats.drop(tpg_stats.tail(1).index, inplace=True)
    tpg_stats['Team'] = [
        re.sub('[^A-Za-z0-9 ]+', '', x) for x in tpg_stats['Team']
    ]
    return tpg_stats
