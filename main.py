import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

import os
import re
import time
import argparse
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
from tensorflow.keras import Sequential, regularizers
from tensorflow.keras.layers import Dense, Dropout
from tensorflow.keras.callbacks import History
from tensorflow.keras.optimizers import Adam
from Global import *
from Services import get_game_df, get_roster_df, get_ppg_df, get_rating_df, get_tpg_df, retrieve_record

YEARS      = ['2017', '2018', '2019', '2020', '2021']       # Year to predict. Will use last year's data
PREV_YEARS = ['2016', '2017', '2018', '2019', '2020']
#YEARS = ['2021']
#PREV_YEARS = ['2020']

DENSE_NEURONS = [1024, 512]
DROPOUT       = 0.3

VALID = 0.98
TEST  = 0.99

def parse_args():
    parser = argparse.ArgumentParser(
        description="Runs an ML model to predict the outcomes of NBA games",
    )

    parser.add_argument(
        "--fetch", "-f", action="store_true", default=False, help="Fetch data from online"
    )
    parser.add_argument("--look_back", "-l", default=50, help="Games to look back on per data point")

    parser.add_argument("--epochs", "-e", default=15, help="Epochs to run ML model for")
    parser.add_argument("--batch_size", "-b", default=5, help="Batch size for ML model")
    parser.add_argument("--learning_rate", "-r", default=.0001, help="Learning rate for ML model")
    parser.add_argument("--team1", help="Team 1 for prediction (use abbreviation)")
    parser.add_argument("--team2", help="Team 1 for prediction (use abbreviation)")
    parser.add_argument("--verbose", "-v", action="store_true", default=False)

    args = parser.parse_args()
    return args


def retrieve_record(games, i, team):
    """ Retrieves record (23, 14) given a game index i """
    current_game = games.iloc[i]
    window       = games[:i]
    last_games = window.loc[(window['Visitor/Neutral'] == team) | (window['Home/Neutral'] == team)]
    wins, losses = 0, 0
    for index, game in last_games.iterrows():
        point_diff = game['PTS1'] - game['PTS2'] if game['Visitor/Neutral'] == team else \
                     game['PTS2'] - game['PTS1']
        if point_diff > 0:
            wins += 1
        else:
            losses += 1
    return wins, losses
    

def create_data_point(all_games, games, ppg_stats, rosters, team_ranks, tpg_stats, game_index, 
                      team1, team2, args, final=False):
    #print("Creating data point for teams {} vs. {}".format(team1, team2))
    """ Creates single data point based on a game """

    # Define window as last i games
    index = all_games[
        (all_games['Date'] == games.iloc[game_index]['Date']) &
        (all_games['Visitor/Neutral'] == games.iloc[game_index]['Visitor/Neutral'])
    ]
    if len(index.index) == 0:
        raise SystemExit("No index found in all_games for date {}, team {}. {}".format(
            games.iloc[game_index]['Date'],
            games.iloc[game_index]['Visitor/Neutral'],
            all_games
        ))
    index = index.index[0]
    window = all_games[:index]
    if not final:
        current_game = games.iloc[game_index]
    input1, input2 = "", ""
    for key, value in abbrevs.items():
        if team1 == value:
            input1 = key
        if team2 == value:
            input2 = key

    ### Visitor win (0) or home win (1)
    if not final:
        label = 0 if current_game['PTS1'] - current_game['PTS2'] > 0 else 1
    else:
        label = None
    
    ### Retrieve last year ranks
    rank1 = team_ranks.loc[team_ranks['Team'] == input1].index[0] + 1
    rank2 = team_ranks.loc[team_ranks['Team'] == input2].index[0] + 1

    ### Retrieve last year team stats
    tpg_stat1 = tpg_stats.loc[tpg_stats['Team'] == input1]
    tpg_stat2 = tpg_stats.loc[tpg_stats['Team'] == input2]
    tpg_stat1.drop(['Team'], axis=1, inplace=True)
    tpg_stat2.drop(['Team'], axis=1, inplace=True)
    tpg_stat1 = tpg_stat1.values.tolist()[0]
    tpg_stat2 = tpg_stat2.values.tolist()[0]

    ### Retrieve last year player stats
    t_stats1 = [
        ppg_stats.loc[ppg_stats['Player'] == rosters[team1]['Starting Lineups']['PG'].iloc[-1]],
        ppg_stats.loc[ppg_stats['Player'] == rosters[team1]['Starting Lineups']['SG'].iloc[-1]],
        ppg_stats.loc[ppg_stats['Player'] == rosters[team1]['Starting Lineups']['C'].iloc[-1]],
        ppg_stats.loc[ppg_stats['Player'] == rosters[team1]['Starting Lineups']['PF'].iloc[-1]],
        ppg_stats.loc[ppg_stats['Player'] == rosters[team1]['Starting Lineups']['SF'].iloc[-1]],
    ]

    t_stats2 = [
        ppg_stats.loc[ppg_stats['Player'] == rosters[team2]['Starting Lineups']['PG'].iloc[-1]],
        ppg_stats.loc[ppg_stats['Player'] == rosters[team2]['Starting Lineups']['SG'].iloc[-1]],
        ppg_stats.loc[ppg_stats['Player'] == rosters[team2]['Starting Lineups']['C'].iloc[-1]],
        ppg_stats.loc[ppg_stats['Player'] == rosters[team2]['Starting Lineups']['PF'].iloc[-1]],
        ppg_stats.loc[ppg_stats['Player'] == rosters[team2]['Starting Lineups']['SF'].iloc[-1]],
    ]
    total_p_stats = []
    for stat in t_stats1:
        stat_list1 = []
        if stat.empty:
            stat_list1 = [0] * 26
        else:
            stat_list1 = stat.iloc[0]
            stat_list1 = stat_list1.drop(['Player', 'Tm', 'Pos'])
            stat_list1 = stat_list1.tolist()
            stat_list1 = [s if s != ' ' else 0 for s in stat_list1]
        total_p_stats += stat_list1

    for stat in t_stats2:
        stat_list2 = []
        if stat.empty:
            stat_list2 = [0] * 26
        else:
            stat_list2 = stat.iloc[0]
            stat_list2 = stat_list2.drop(['Player', 'Tm', 'Pos'])
            stat_list2 = stat_list2.tolist()
            stat_list2 = [s if s != ' ' else 0 for s in stat_list2]
        total_p_stats += stat_list2

    ### This year last X Games
    games1 = window.loc[(window['Visitor/Neutral'] == input1) | (window['Home/Neutral'] == input1)]
    games2 = window.loc[(window['Visitor/Neutral'] == input2) | (window['Home/Neutral'] == input2)]
    point_diff1, point_diff2, opponent_wins1, opponent_losses1, opponent_wins2, opponent_losses2 = [], [], [], [], [], []
    for index, game in games1.iterrows():
        point_diff1.append(
            game['PTS1'] - game['PTS2'] if game['Visitor/Neutral'] == input1 else \
            game['PTS2'] - game['PTS1']
        )
        # Opponent Record so far this year
        opponent_wins = game['Wins1'] if game['Home/Neutral'] == input1 else game['Wins2']
        opponent_losses = game['Losses1'] if game['Home/Neutral'] == input1 else game['Losses2']
        opponent_wins1.append(opponent_wins)
        opponent_losses1.append(opponent_losses)
    for index, game in games2.iterrows():
        point_diff2.append(
            game['PTS1'] - game['PTS2'] if game['Visitor/Neutral'] == input2 else \
            game['PTS2'] - game['PTS1']
        )
        # Opponent Record so far this year
        opponent_wins = game['Wins1'] if game['Home/Neutral'] == input2 else game['Wins2']
        opponent_losses = game['Losses1'] if game['Home/Neutral'] == input2 else game['Losses2']
        opponent_wins2.append(opponent_wins)
        opponent_losses2.append(opponent_losses)

    # Account for look back (only take X last games)
    point_diff1 = point_diff1[-args.look_back:]
    point_diff2 = point_diff2[-args.look_back:]
    opponent_wins1 = opponent_wins1[-args.look_back:]
    opponent_losses1 = opponent_losses1[-args.look_back:]
    opponent_wins2 = opponent_wins2[-args.look_back:]
    opponent_losses2 = opponent_losses2[-args.look_back:]

    #print("Opponent wins, losses: {} {}".format(opponent_wins1, opponent_losses1))

    # Record so far for this year
    current_wins1, current_losses1 = retrieve_record(games, game_index, input1)
    current_wins2, current_losses2 = retrieve_record(games, game_index, input2)
    #print("Current wins, losses: {} {}".format(current_wins1, current_losses1))


    # Pad with 0's if not enough games
    if args.look_back > len(point_diff1):
        point_diff1 += [0] * (args.look_back - len(point_diff1))
        opponent_wins1 += [0] * (args.look_back - len(opponent_wins1))
        opponent_losses1 += [0] * (args.look_back - len(opponent_losses1))
    if args.look_back > len(point_diff2):
        point_diff2 += [0] * (args.look_back - len(point_diff2))
        opponent_wins2 += [0] * (args.look_back - len(opponent_wins2))
        opponent_losses2 += [0] * (args.look_back - len(opponent_losses2))

    ### Concatenate all items into a list
    data_point = [rank1, rank2] + \
                 tpg_stat1 + tpg_stat2 + \
                 point_diff1 + point_diff2 + \
                 [current_wins1] + [current_losses1] + \
                 [current_wins2] + [current_losses2] + \
                 opponent_wins1 + opponent_losses1 + \
                 opponent_wins2 + opponent_losses2 + \
                 total_p_stats

    return data_point, label


def create_data(years, prev_years, args):
    """ Creates data points from years """
    x, y = [], []

    # Fetch all games first
    print("> Fetching all games from {}-{}".format(years[0], years[-1]))
    if os.path.exists('data/all_games.csv'):
        all_games = pd.read_csv('data/all_games.csv')
    else:
        all_games      = get_game_df(years, args)
        all_games.to_csv('data/all_games.csv', index=False)

    for year, prev_year in zip(years, prev_years):
        # Load dfs if exist
        if os.path.exists('data/games_' + year + '.csv'):
            print("> Loading dataframes from file")
            games = pd.read_csv('data/games_' + year + '.csv')
            ppg_stats = pd.read_csv('data/ppg_stats_' + prev_year + '.csv')
            rosters = {}
            for team, abbrev in abbrevs.items():
                rosters[abbrev] = {}
                rosters[abbrev]['Starting Lineups'] = pd.read_csv('data/rosters_' + abbrev + '_' + year + '.csv')
            team_ranks = pd.read_csv('data/team_ranks_' + prev_year + '.csv')
            tpg_stats = pd.read_csv('data/tpg_stats_' + prev_year + '.csv')
            
        else:
            # Fetch dfs
            games       = get_game_df([year], args)
            ppg_stats   = get_ppg_df(prev_year, args)
            rosters     = get_roster_df(year, ppg_stats, args)
            team_ranks  = get_rating_df(prev_year, args)
            tpg_stats   = get_tpg_df(prev_year, args)

            # Save dfs for later
            print("> Saving dataframes to file")
            games.to_csv('data/games_' + year + '.csv', index=False)
            ppg_stats.to_csv('data/ppg_stats_' + prev_year + '.csv', index=False)
            for abbrev, roster in rosters.items():
                roster['Starting Lineups'].to_csv('data/rosters_' + abbrev + "_" + year + '.csv', index=False)
            team_ranks.to_csv('data/team_ranks_' + prev_year + '.csv', index=False)
            tpg_stats.to_csv('data/tpg_stats_' + prev_year + '.csv', index=False)

        ### Create data points
        #- Input -> 
        #    Last Year Rank
        #    Last Year Team Stats 
        #    X (Ignore for now) Last Year Players Stats 
        #    Current Year Team Stats Last X Games, each with their own record of the current season, 0 if not part of current season (have function for finding record from games + index)
        #    Current Record
        for i in range(args.look_back, len(games.index)):
            print("> Creating data point for year {} ({}/{})".format(
                year, i+1-args.look_back, len(games) - args.look_back), end='\r'
            )

            # Define window as last i games
            current_game = games.iloc[i]
            input1 = current_game['Visitor/Neutral']
            input2 = current_game['Home/Neutral']
            team1  = abbrevs[input1]
            team2  = abbrevs[input2]
            data_point, label = create_data_point(   
                all_games, games, ppg_stats, rosters, team_ranks, tpg_stats, i, team1, team2, args, final=False
            )
            x.append(data_point)
            y.append(label)

        print("")

    
    x = [[float(a) for a in b] for b in x]
    uniques = set([len(a) for a in x])
    print(uniques)
        
    y = [float(b) for b in y]
    x = np.array(x, dtype=object)
    y = np.array(y, dtype=object)

    return x, y

def create_final_data_point(args, year, prev_year):
    print("> Creating final data point between teams {} and {}".format(args.team1, args.team2))
    all_games = pd.read_csv('data/all_games.csv')
    games     = pd.read_csv('data/games_' + year + '.csv')
    ppg_stats = pd.read_csv('data/ppg_stats_' + prev_year + '.csv')
    rosters   = {}
    for team, abbrev in abbrevs.items():
        rosters[abbrev] = {}
        rosters[abbrev]['Starting Lineups'] = pd.read_csv('data/rosters_' + abbrev + '_' + year + '.csv')
    team_ranks = pd.read_csv('data/team_ranks_' + prev_year + '.csv')
    tpg_stats  = pd.read_csv('data/tpg_stats_' + prev_year + '.csv')
    final_data_point, _ = create_data_point(   
        all_games, games, ppg_stats, rosters, team_ranks, tpg_stats, 
        len(games.index)-1, args.team1, args.team2, args, final=True
    )
    final_data_point = [float(x) for x in final_data_point]
    normalize([final_data_point])
    final_data_point = np.array([final_data_point])
    return final_data_point


def split_data(x, y):
    """ Splits data -> train, valid, test """
    start_valid = int(len(x) * VALID)
    start_test  = int(len(x) * TEST)
    train_x, train_y = x[0:start_valid], y[0:start_valid]
    valid_x, valid_y = x[start_valid:start_test], y[start_valid:start_test]
    test_x, test_y   = x[start_test:], y[start_test:]
    return train_x, train_y, valid_x, valid_y, test_x, test_y

def normalize(arr):
    for i in range(len(arr)):
        row = arr[i]
        rnge = max(row) - min(row)
        mn = min(row)
        for j in range(len(arr[i])):
            arr[i][j] = (arr[i][j] - mn) / rnge


def ml_model(dim, train_x, train_y, valid_x, valid_y, test_x, test_y, args):
    """ 
    1. Create + compile model
    2. Fit model on training + validation data
    3. Predict using testing data
    """
    # Create model
    model = Sequential()
    model.add(Dense(dim, input_dim=dim, activation='relu'))
    for x in DENSE_NEURONS:
        model.add(Dense(x, activation='relu', 
            kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4),
            bias_regularizer=regularizers.l2(1e-4),
            activity_regularizer=regularizers.l2(1e-5)
                
        ))
        if DROPOUT > 0:
            model.add(Dropout(DROPOUT))
    model.add(Dense(1, activation='sigmoid'))

    # Compile model
    optimizer = Adam(
        learning_rate = float(args.learning_rate),
        beta_1 = 0.9,
        beta_2 = 0.999,
        epsilon = 1e-08,
        amsgrad = False,
        name="Adam"
    )
    model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])
    #model.compile(loss='hinge', optimizer=optimizer, metrics=['accuracy'])

    # Fit model
    history = History()
    model.fit(
        train_x, 
        train_y, 
        validation_data=(valid_x, valid_y), 
        epochs=int(args.epochs), 
        batch_size=int(args.batch_size), 
        callbacks=[history]
    )

    # Predict
    loss, accuracy = model.evaluate(test_x, test_y)
    predictions    = model.predict(test_x)
    print("--- Test ---")
    print("Loss: {}".format(loss))
    print("Accuracy: {}".format(accuracy))

    # Predict Next Game
    if args.team1 and args.team2:
        final_data_point = create_final_data_point(args, YEARS[-1], PREV_YEARS[-1])
        print(test_x[0])
        print(final_data_point[0])
        predictions = model.predict(final_data_point)
        print("Prediction: {}".format(predictions))

    # Plot Accuracy
    plt.plot(history.history['accuracy'])
    plt.plot(history.history['val_accuracy'])
    plt.xlabel('epoch')
    plt.ylabel('accuracy')
    plt.legend(['training', 'validation'], loc='upper left')
    plt.show()

    # Plot Loss 
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.xlabel('epoch')
    plt.ylabel('loss')
    plt.legend(['training', 'validation'], loc='upper left')
    plt.show()
    return predictions


def main():
    # Set pandas options
    #pd.set_option("display.max_rows", None)
    #pd.set_option("display.max_columns", None)
    pd.options.mode.chained_assignment = None

    # Get args
    args = parse_args()
    csv_files = ['train_x.csv', 'train_y.csv', 'valid_x.csv', 'valid_y.csv', 'test_x.csv', 'test_y.csv']

    # Fetch data
    if args.fetch:
        print("--- Creating data ---")
        x, y = create_data(YEARS, PREV_YEARS, args)

        print("--- Normalizing data ---")
        #x = x.astype(np.float)
        #y = y.astype(np.float)
        normalize(x)

        print("--- Saving Data ---")
        np.savetxt('data/x.csv', x, delimiter=',')
        np.savetxt('data/y.csv', y, delimiter=',')
        train_x, train_y, valid_x, valid_y, test_x, test_y = split_data(x, y)

    else:
        print("--- Fetching Data ---")
        x = np.loadtxt('data/x.csv', delimiter=',')
        y = np.loadtxt('data/y.csv', delimiter=',')
        train_x, train_y, valid_x, valid_y, test_x, test_y = split_data(x, y)

    # Run ML model, and retrieve predictions
    print("--- Running ML model ---")
    print("> Running with parameters: e={}, l={}, lr={}, b={}".format(args.epochs, args.look_back, args.learning_rate, args.batch_size))
    print("> Training split: {} / {} / {}".format(len(train_x), len(valid_x), len(test_x)))
    predictions = ml_model(len(train_x[0]), train_x, train_y, valid_x, valid_y, test_x, test_y, args)

    # Print predictions of latest year (and whether they were correct or not)
    #correct = []
    #right, wrong = 0, 0
    #for answer, guess in zip(test_y, predictions):
    #    if guess > 0.5 and answer == 1 or guess <= 0.5 and answer == 0:
    #        correct.append("\033[1;32;40m RIGHT\033[1;30;40m")
    #        right += 1
    #    else:
    #        correct.append("\033[1;31;40m WRONG\033[1;30;40m")
    #        wrong += 1

    #for i, prediction in enumerate(predictions):
    #    value = prediction[0]
    #    guess = 'Team 1 Wins' if value < 0.5 else 'Team 2 Wins' 
    #    odds  = value/0.5 if value > 0.5 else (1-value)/0.5
        #print("{} (Odds: {:0.2f}:{}) {} | {} vs. {} on {}".format(
        #    guess, odds, '1', correct[i], test_games.loc[i+args.look_back, 'Visitor/Neutral'],
        #    test_games.loc[i+args.look_back, 'Home/Neutral'], 
        #    test_games.loc[i+args.look_back, 'Date']
        #))

if __name__ == '__main__':
    main()
