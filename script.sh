#!/bin/bash

#for e in 1 5 10 15 20 30
#do
#    for l in 10 20 30 40 50
#    do
#        for lr in 0.001 0.0005 0.0002 0.0001 0.00005 0.00001
#        do
#            for b in 1 2 5 10 20 30
#            do
#                echo "Running for e=$e, l=$l, lr=$lr, b=$b..."
#                python3 main.py -e ${e} -l ${l} -r ${lr} -b ${b} | tee -a output.txt
#            done
#        done
#    done
#done
b=5
l=30
e=200
for lr in 0.00075
do
    echo "Running for e=$e, l=$l, lr=$lr, b=$b..."
    python3 main.py -e ${e} -l ${l} -r ${lr} -b ${b} | tee -a output.txt
done
